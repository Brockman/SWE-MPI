##################################################
 Here lies the code/executables/in-and-out.data: #
##################################################

./implementation/*: 
	The C++/MAKE/etc. files to 1. make, 2. compile, 3. execute for simulating SWE, optimised via LTS and distributed-memory parallelised by MPI.

./presentation_swe/*  ./data/*:
	Output data, results images and .mp4 video files to showcase results 

