############################################################################################################
HPC-LEAP WP4 Trinity College Dublin - Tsunami simulation via SWE optimised w/ hLTS and parallelised w/ MPI
############################################################################################################

Some code (SWE_*), a presentation (*.pdf) and their backups for running a nice tsunami simulator lie within. 

./SWE_*/*:
	C++ code for running the simulation, and the results we produced: mp4 video files, images, out.dat, etc.
	FOR MORE VERBOSE DETAILS, SEE: ./SWE_*/README_code.md.

./*.pdf:
	Nice presentation to show it all off (latex-->pdf).

./backup/*:
	some tar dumps.



